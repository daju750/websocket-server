console.log('Escritorio HTML');
const lblEscritorio = document.querySelector('#lblescritorio');
const btnAtender = document.querySelector('#btnAtender');
const lblTicket = document.querySelector('#lblTicket');
const alert = document.querySelector('#alert');
const lblPendientes = document.querySelector('#lblPendientes');

const searchParams = new URLSearchParams(window.location.search);

if(!searchParams.has('escritorio')){
    window.location = 'index.html';
    throw new Error("El escritorio es obligatorio");
}

const escritorio = searchParams.get('escritorio');
lblEscritorio.innerText = escritorio;

alert.style.display = 'none';

const socket = io();

socket.on('connect', () => {btnAtender.disabled = false;});
socket.on('disconnect', () => {btnAtender.disabled = true;});

socket.on('tickets-pendientes',(payload)=>{
    lblPendientes.innerText = payload;
})

btnAtender.addEventListener('click',() => {

    socket.emit('atender-ticket',{escritorio},(payload) => {
        
        if(!payload.ok){
            lblTicket.innerText = 'Sin Tickets';
            return alert.style.display = '';
        }
        console.log(payload)
        lblTicket.innerText = 'Ticket: ' + payload.ticket.numero;

    });


});