console.log('Nuevo Ticket HTML');
const lblNuevoTicket=document.querySelector('#lblNuevoTicket');
const btnCrear = document.querySelector('button');

const socket = io();

socket.on('connect', () => {btnCrear.disabled = false;});

socket.on('disconnect', () => {btnCrear.disabled = true;});

socket.on('ultimo-ticket',(ultimo)=>{
    lblNuevoTicket.innerText = "Ticket: " + ultimo;
});

btnCrear.addEventListener('click',(payload) => {

    socket.emit( 'siguiente-ticket',payload, (ticket) => {
        lblNuevoTicket.innerText = ticket;
    });

});